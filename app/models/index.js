const dbConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.tutorials = require("./tutorial.model.js")(mongoose, mongoosePaginate);
db.logs = require("./log.model.js")(mongoose, mongoosePaginate);
db.customers = require("./customer.model.js")(mongoose, mongoosePaginate);
db.questions = require("./question.model.js")(mongoose, mongoosePaginate);
db.results = require("./result.model.js")(mongoose, mongoosePaginate);
module.exports = db;
