module.exports = (mongoose, mongoosePaginate) => {
  var schema = mongoose.Schema(
    {
      user: String,
      text: String,
      datetime: String,
      type: String
    },
    { timestamps: true }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  schema.plugin(mongoosePaginate);

  const Log = mongoose.model("log", schema);
  return Log;
};