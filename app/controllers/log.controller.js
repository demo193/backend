const db = require("../models");
const Log = db.logs;

const getPagination = (page, size) => {
  const limit = size ? +size : 15;
  const offset = page ? page * limit : 0;

  return { limit, offset };
};

// Create and Save a new Log
exports.create = (req, res) => {
  // Validate request
  if (!req.body.user) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Log
  const log = new Log({
    user: req.body.user,
    text: req.body.text,
    type: req.body.type,
  });

  // Save Log in the database
  log
    .save(log)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Log.",
      });
    });
};

// Retrieve all Logs from the database.
exports.findAll = (req, res) => {
  const { page, size, user } = req.query;
  var condition = user
    ? { user: { $regex: new RegExp(user), $options: "i" } }
    : {};

  const { limit, offset } = getPagination(page, size);

  Log.paginate(condition, { offset, limit })
    .then((data) => {
      res.send({
        totalItems: data.totalDocs,
        logs: data.docs,
        totalPages: data.totalPages,
        currentPage: data.page - 1,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving logs.",
      });
    });
};

// Find a single Log with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Log.findById(id)
    .then((data) => {
      if (!data)
        res.status(404).send({ message: "Not found Customer with id " + id });
      else res.send(data);
    })
    .catch((err) => {
      res
        .status(500)
        .send({ message: "Error retrieving Customer with id=" + id });
    });
};

// Update a Log by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  Log.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Log with id=${id}. Maybe Log was not found!`,
        });
      } else res.send({ message: "Log was updated successfully." });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Log with id=" + id,
      });
    });
};

// Delete a Log with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Log.findByIdAndRemove(id, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Log with id=${id}. Maybe Log was not found!`,
        });
      } else {
        res.send({
          message: "Log was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Log with id=" + id,
      });
    });
};

// Delete all Logs from the database.
exports.deleteAll = (req, res) => {
  Log.deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} Logs were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all logs.",
      });
    });
};
