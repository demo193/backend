module.exports = app => {
    const result = require("../controllers/result.controller.js");
  
    var router = require("express").Router();

    // Retrieve all Logs
    router.get("/", result.findAll);
  
  
  
    app.use("/api/result", router);
  };